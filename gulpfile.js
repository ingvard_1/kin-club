let
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    babel = require('gulp-babel');

gulp.task('sass', function () {
    return gulp.src('app/sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/css/'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('deployCss', function () {
    return gulp.src('app/css/style.css')
        .pipe(autoprefixer(['last 15 versions'], {cascade: true}))
        .pipe(cssnano())
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('app/css/'));
});

gulp.task('scripts', function () {
    return gulp.src([
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap/tab.js',
        'app/libs/smoothScroll.js',
        'popup/popup.js',
        'app/libs/wow.js',
        'app/js/common.js'
    ])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('app/js/'));
});

gulp.task('deployScripts', function () {
    return gulp.src('all.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(rename('all.min.js'))
        .pipe(gulp.dest('app/js/'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: 'app/'
        },
        notify: false
    });
});

gulp.task('build', ['deployCss', 'deployScripts']);

gulp.task('watch', ['browserSync'], function () {
    gulp.watch('app/sass/*.scss', ['sass']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/*.js', ['scripts'], browserSync.reload);
});
